import java.util.Scanner;

public class Encrypt {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String word = sc.nextLine();
        StringBuilder decrypted = new StringBuilder();

        for(int i = 0; i < word.length(); i++){
            char c = (char) (word.charAt(i) - 1);
            decrypted.append(c);
        }
        System.out.println(decrypted.toString());
    }
}
