import java.util.Scanner;

public class Eolymp20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        long n = sc.nextLong();

        int count = 1;
        while (n > 9) {
            long sum = 0;
            long temp = n;
            while (temp > 0) {
                sum += temp % 10;
                temp /= 10;
            }
            n -= sum;
            count++;
        }

        System.out.println(count);
    }
}
